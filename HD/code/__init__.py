"""
TEXAS HOLD’EM - analysis of winning likelihood

Main reason of the project is to conduct simulation  of example play   of  card
game Poker Texas Hold'em. During simulation application will compute likelihood
of winning.

Prepared for the Software Engineering course, Mathematics, Gdansk University of
Technology.

Copyright (C) 2018 by Filip Szmid, Uladzislau Paulouski, Marcin Konopka.

This  program  is free software: you can redistribute it and/or modify it under
the terms of the  GNU  General Public License as published by the Free Software
Foundation, either  version  3 of  the  License,  or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even  the  implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.

See the GNU General Public License for more details,available at
<https://www.gnu.org/licenses/>.

Started on October 16, 2018. Last revision: October 30, 2018.

acknowledgements:
https://www.pexels.com - picture in summary report
https://logo-app.ucraft.com - starting screen logo
https://www.iconfinder.com - starting screen icon
https://code.google.com/archive/p/vector-playing-cards/ - card assets
"""

from HD.code.functions import *
from HD.code.compute_likelihood import *


# this is main function responsible for managing game stages
# and displaying game board
def display_game_screen():
    game_running = True  # game will end when it takes value False

    # variables used to control content displayed on screen
    display_stages = [True, False, False, False, False]
    round_tick = 0
    joke_tick = 0

    # variables when we will keep computed probabilities
    winning_rate = 50.0
    draw_rate = '?'

    draw_cards()

    while game_running:

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                game_running = False

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    game_running = False

                if event.key == pygame.K_i:

                    # performing simulation for additional stats
                    if (round_tick > 0):
                        simulation_results = compute_winning_likelihood(cards_on_table, round_tick)
                    else:
                        simulation_results = ['?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?']

                    event = display_info(event, simulation_results)

                if event.key == pygame.K_j:
                    event = display_joke(event, joke_tick)
                    joke_tick = joke_tick + 1

                if event.key == pygame.K_SPACE:

                    # calling fuction which returns probabilities and storing it to variables
                    # for later display
                    simulation_results = compute_winning_likelihood(cards_on_table, round_tick)
                    winning_rate = simulation_results[0]
                    draw_rate = simulation_results[1]

                    round_tick = round_tick + 1

                    if round_tick <= 4:
                        display_stages[round_tick] = True

                    else:
                        # setting everything to default
                        draw_cards()
                        display_stages = [True, False, False, False, False]
                        winning_rate = 50.0
                        draw_rate = '?'
                        round_tick = 0

        # with every iteration we clear screen and then draw
        # new state of the board
        gameWindow.fill(WHITE)

        if (display_stages[0] == True):
            display_background()

            print_msg_on_screen("Probability that you will win is " + str(winning_rate) + " %", SCREEN_W / 2 - 350,
                                SCREEN_H / 2 - MARGIN)
            print_msg_on_screen(
                "Probability that your opponent will win is " + str(round(100 - winning_rate, 2)) + " %",
                SCREEN_W / 2 - 400, SCREEN_H / 2 + MARGIN)
            print_msg_on_screen("Probability of draw is " + str(draw_rate) + " %", SCREEN_W / 2 - 250,
                                SCREEN_H / 2 + 3 * MARGIN)

        if (display_stages[1] == True):
            # displaying player's hand
            gameWindow.blit(cards_images[0], (MARGIN, MARGIN))
            gameWindow.blit(cards_images[1], (MARGIN, 2 * MARGIN + CARD_H))

            # displaying enemy's hand
            gameWindow.blit(cards_images[2], (SCREEN_W - MARGIN - CARD_W, MARGIN))
            gameWindow.blit(cards_images[3], (SCREEN_W - MARGIN - CARD_W, 2 * MARGIN + CARD_H))

        if (display_stages[2] == True):
            # flop
            gameWindow.blit(cards_images[4], (SCREEN_W / 2 - 500, MARGIN))
            gameWindow.blit(cards_images[5], (SCREEN_W / 2 - 300, MARGIN))
            gameWindow.blit(cards_images[6], (SCREEN_W / 2 - 100, MARGIN))

        if (display_stages[3] == True):
            # turn
            gameWindow.blit(cards_images[7], (SCREEN_W / 2 + 100, MARGIN))

        if (display_stages[4] == True):
            # river
            gameWindow.blit(cards_images[8], (SCREEN_W / 2 + 300, MARGIN))

        pygame.display.update()


def main():
    pygame.init()
    pygame.display.set_caption("Texas Hold'em")
    icon = pygame.image.load('../textures/cards/ace_of_clubs.png')
    pygame.display.set_icon(icon)

    display_welcome_screen()
    display_game_screen()

    pygame.quit()
    quit()


if __name__ == '__main__':
    main()
