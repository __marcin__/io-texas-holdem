from SD.code.__init__ import *
from ast import literal_eval
from random import randint

import pygame

# -----------------------------------------------------------
# global variables
# -----------------------------------------------------------

SCREEN_W = 1280
SCREEN_H = 720
MARGIN = 50

CARD_W = int(500 / 4)
CARD_H = int(726 / 4)

ST_SCR_W = 850
ST_SCR_H = 670

WHITE = (255, 255, 255)
BLACK = (0, 0, 0,)

gameWindow = pygame.display.set_mode((SCREEN_W, SCREEN_H))

starting_screen_image = pygame.image.load('../textures/starting_screen.png')
frame = pygame.image.load('../textures/frame.png')
frame = pygame.transform.scale(frame, (CARD_W, CARD_H))

with open('../textures/cards_paths.txt') as f:
    cards_paths = [list(literal_eval(line)) for line in f]

cards_images = [None] * 9  # vector in which we will keep card images
cards_on_table = [None] * 9  # vector in which we will keep card numbers

mems = [None] * 6  # vector in which we will keep mem images

mems[0] = pygame.image.load('../textures/mems/mem0.jpg')
mems[1] = pygame.image.load('../textures/mems/mem1.jpg')
mems[2] = pygame.image.load('../textures/mems/mem2.jpg')
mems[3] = pygame.image.load('../textures/mems/mem3.jpg')
mems[4] = pygame.image.load('../textures/mems/mem4.jpg')
mems[5] = pygame.image.load('../textures/mems/mem5.jpg')


# -----------------------------------------------------------
# utilities
# -----------------------------------------------------------

def draw_cards():
    global cards_images

    for i in range(0, 9):
        cards_on_table[i] = randint(0, 51)

        cards_images[i] = pygame.image.load('../textures/cards/' + cards_paths[cards_on_table[i]][1] + '.png')
        cards_images[i] = pygame.transform.scale(cards_images[i], (CARD_W, CARD_H))


def print_msg_on_screen(msg, x, y, color=BLACK, font_size=15):
    font = pygame.font.SysFont("Courier New", font_size)
    screen_text = font.render(msg, True, color)
    gameWindow.blit(screen_text, [x, y])


# -----------------------------------------------------------
# secondary screens
# -----------------------------------------------------------

def display_welcome_screen():
    intro = True

    while intro:

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.quit()
                    quit()
                else:
                    intro = False

        gameWindow.fill(WHITE)
        gameWindow.blit(starting_screen_image, (SCREEN_W / 2 - ST_SCR_W / 2, 2 * MARGIN))
        pygame.display.update()


def display_background():
    gameWindow.blit(frame, (MARGIN, MARGIN))
    gameWindow.blit(frame, (MARGIN, 2 * MARGIN + CARD_H))
    gameWindow.blit(frame, (SCREEN_W - MARGIN - CARD_W, MARGIN))
    gameWindow.blit(frame, (SCREEN_W - MARGIN - CARD_W, 2 * MARGIN + CARD_H))

    gameWindow.blit(frame, (SCREEN_W / 2 - 200 - 50, MARGIN))
    gameWindow.blit(frame, (SCREEN_W / 2 - 100 - 50, MARGIN))
    gameWindow.blit(frame, (SCREEN_W / 2 - 50, MARGIN))
    gameWindow.blit(frame, (SCREEN_W / 2 + 100 - 50, MARGIN))
    gameWindow.blit(frame, (SCREEN_W / 2 + 200 - 50, MARGIN))
    print_msg_on_screen("Your hand", CARD_W / 2 - 30, 3 * MARGIN + 2 * CARD_H)
    print_msg_on_screen("Villain hand", SCREEN_W - CARD_W / 2 - 130, 3 * MARGIN + 2 * CARD_H)
    print_msg_on_screen("[ <space> proceed]", 20, SCREEN_H - 2 * MARGIN)
    print_msg_on_screen("[ hold <i> additional stats]", 8 * MARGIN, SCREEN_H - 2 * MARGIN)
    print_msg_on_screen("[ hold <j> joke]", 16 * MARGIN, SCREEN_H - 2 * MARGIN)
    print_msg_on_screen("[ <ESC> quit]", SCREEN_W - 3 * MARGIN, SCREEN_H - 2 * MARGIN)


def display_info(event, sim_res):
    subscreen_running = True

    while subscreen_running:
        gameWindow.fill(WHITE)

        print_msg_on_screen("<probability of final hand>", SCREEN_W / 2 - 80, SCREEN_H / 3 - 200)
        print_msg_on_screen("Probability of four of kind: " + str(sim_res[2]) + " %", SCREEN_W / 2 - 110,
                            SCREEN_H / 2 - MARGIN - 200)
        print_msg_on_screen("Probability of full: " + str(sim_res[3]) + " %", SCREEN_W / 2 - 110, SCREEN_H / 2 - 200)
        print_msg_on_screen("Probability of color:" + str(sim_res[4]) + " %", SCREEN_W / 2 - 110,
                            SCREEN_H / 2 + MARGIN - 200)
        print_msg_on_screen("Probability of street: " + str(sim_res[5]) + " %", SCREEN_W / 2 - 110,
                            SCREEN_H / 2 + 2 * MARGIN - 200)
        print_msg_on_screen("Probability of trips: " + str(sim_res[6]) + " %", SCREEN_W / 2 - 110,
                            SCREEN_H / 2 + 3 * MARGIN - 200)
        print_msg_on_screen("Probability of two pairs: " + str(sim_res[7]) + " %", SCREEN_W / 2 - 110,
                            SCREEN_H / 2 + 4 * MARGIN - 200)
        print_msg_on_screen("Probability of one pair:  " + str(sim_res[8]) + " %", SCREEN_W / 2 - 110,
                            SCREEN_H / 2 + 5 * MARGIN - 200)
        print_msg_on_screen("Probability of high card: " + str(sim_res[9]) + " %", SCREEN_W / 2 - 110,
                            SCREEN_H / 2 + 6 * MARGIN - 200)

        pygame.display.update()

        for event in pygame.event.get():
            if event.key == pygame.K_i:
                subscreen_running = False
    return event


def display_joke(event, joke_tick):
    subscreen_running = True
    while subscreen_running:
        gameWindow.fill(WHITE)

        if joke_tick % 6 == 0:
            gameWindow.blit(mems[0], (SCREEN_W / 2 - 200, 1.5 * MARGIN))
        elif joke_tick % 6 == 1:
            gameWindow.blit(mems[1], (SCREEN_W / 2 - 200, 1.5 * MARGIN))

        elif joke_tick % 6 == 2:
            gameWindow.blit(mems[2], (SCREEN_W / 2 - 200, 1.5 * MARGIN))

        elif joke_tick % 6 == 3:
            gameWindow.blit(mems[3], (SCREEN_W / 2 - 200, 1.5 * MARGIN))

        elif joke_tick % 6 == 4:
            gameWindow.blit(mems[4], (SCREEN_W / 2 - 200, 1.5 * MARGIN))

        elif joke_tick % 6 == 5:
            gameWindow.blit(mems[5], (SCREEN_W / 2 - 200, 1.5 * MARGIN))

        pygame.display.update()

        for event in pygame.event.get():
            if event.key == pygame.K_j:
                subscreen_running = False
    return event
