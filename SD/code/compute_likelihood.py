from SD.code.translate import *
from SD.simulations.próby import prawdopodobienstwo


def compute_winning_likelihood(cards_on_table, round_tick):
    # -----------------------------------------------------
    # translation of card indexing
    # -----------------------------------------------------
    matrix = [[0, 0],
              [0, 0],
              [0, 0],
              [0, 0],
              [0, 0],
              [0, 0],
              [0, 0],
              [0, 0],
              [0, 0]]

    for i in range(0, 8):
        matrix[i] = translate(cards_on_table[i])

    g1 = [matrix[0],
          matrix[1],
          matrix[4],
          matrix[5],
          matrix[6],
          matrix[7],
          matrix[8]]

    g2 = [matrix[2],
          matrix[3],
          matrix[4],
          matrix[5],
          matrix[6],
          matrix[7],
          matrix[8]]

    # -----------------------------------------------------
    # calling function which will perform simulations
    # -----------------------------------------------------

    t = prawdopodobienstwo(g1, g2, round_tick, 10000)

    return (t)
