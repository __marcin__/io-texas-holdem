import random


def para(board, wagareki):
    pary = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    znak = 0
    a = 0
    b = board
    hajboard = board
    hajboard.sort(reverse=True)

    kicker = 0
    kicker1 = 0
    kicker2 = 0

    wagareki = [0, hajboard[0][0], hajboard[1][0], hajboard[2][0], hajboard[3][0], hajboard[4][0], hajboard[5][0]]

    for x in range(0, 7):
        if (board[x][0] == 2): pary[0] = pary[0] + 1
        if (board[x][0] == 3): pary[1] = pary[1] + 1
        if (board[x][0] == 4): pary[2] = pary[2] + 1
        if (board[x][0] == 5): pary[3] = pary[3] + 1
        if (board[x][0] == 6): pary[4] = pary[4] + 1
        if (board[x][0] == 7): pary[5] = pary[5] + 1
        if (board[x][0] == 8): pary[6] = pary[6] + 1
        if (board[x][0] == 9): pary[7] = pary[7] + 1
        if (board[x][0] == 10): pary[8] = pary[8] + 1
        if (board[x][0] == 11): pary[9] = pary[9] + 1
        if (board[x][0] == 12): pary[10] = pary[10] + 1
        if (board[x][0] == 13): pary[11] = pary[11] + 1
        if (board[x][0] == 14): pary[12] = pary[12] + 1

    for i in range(0, 13):

        if (pary[i] == 2 and znak < 1):

            for z in range(0, 7):
                if (board[z][0] != i + 2 and kicker < board[z][0]): kicker = board[z][0]
            for z in range(0, 7):
                if (board[z][0] != i + 2 and kicker1 < board[z][0] and kicker != board[z][0]): kicker1 = board[z][0]

            for z in range(0, 7):
                if (board[z][0] != i + 2 and kicker2 < board[z][0] and kicker != board[z][0] and kicker1 != board[z][
                    0]): kicker2 = board[z][0]

            wagareki = [1, i + 2, kicker, kicker1, kicker2, 0]
            for k in range(0, 13):
                if (pary[k] > 1 and znak < 2 and k != i):
                    kicker = 0
                    # DWIE PARY
                    for z in range(0, 7):
                        if (board[z][0] != i + 2 and board[z][0] != k + 2 and kicker < board[z][0]):
                            kicker = board[z][0]
                    if (k > i): wagareki = [2, k + 2, i + 2, kicker, 0, 0, 0]
                    if (k <= i): wagareki = [2, i + 2, k + 2, kicker, 0, 0]

        if (pary[i] == 3 and znak < 3):
            # TROJKA
            for z in range(0, 7):
                if (board[z][0] != i + 2 and kicker < board[z][0]): kicker = board[z][0]
            for z in range(0, 7):
                if (board[z][0] != i + 2 and kicker1 < board[z][0] and kicker != board[z][0]): kicker1 = board[z][0]

            wagareki = [3, i + 2, kicker, kicker1, 0, 0]

            for j in range(0, 13):
                if (pary[j] >= 2 and j != i and znak < 7):
                    # FULLHOUSE
                    if (i > j): wagareki = [7, i + 2, j + 2, 0, 0, 0]
                    if (i <= j and pary[j] < 3): wagareki = [7, i + 2, j + 2, 0, 0, 0]
                    if (j >= i and pary[j] > 2): wagareki = [7, j + 2, i + 2, 0, 0, 0]

        if (pary[i] == 4 and znak < 8):
            # KARETA
            for z in range(0, 7):
                if (board[z][0] != i + 2 and kicker < board[z][0]): kicker = board[z][0]
            wagareki = [8, i + 2, kicker, 0, 0, 0]

        if (znak < wagareki[1]):
            znak = wagareki[0]

    return wagareki
