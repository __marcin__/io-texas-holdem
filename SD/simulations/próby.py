from SD.simulations.color import *
from SD.simulations.pary import *
from SD.simulations.straight3 import straight3


def prawdopodobienstwo(g1, g2, znacznik, N):
    z1 = 0
    z2 = 0
    u01 = 0
    u11 = 0
    u21 = 0
    u31 = 0
    u41 = 0
    u51 = 0
    u61 = 0
    u71 = 0
    u81 = 0
    u91 = 0
    u02 = 0
    u12 = 0
    u22 = 0
    u32 = 0
    u42 = 0
    u52 = 0
    u62 = 0
    u72 = 0
    u82 = 0
    u92 = 0
    remis = 0
    wagareki = [0, 0, 0, 0, 0, 0]
    x1 = [[0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0]]
    x2 = [[0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0]]
    pwing1 = 0
    pwing2 = 0
    prem = 0

    for i in range(0 + 1, N + 1):

        if (znacznik == 0):
            x1[0][0] = g1[0][0]
            x1[0][1] = g1[0][1]
            x1[1][0] = g1[1][0]
            x1[1][1] = g1[1][1]

            x2[0][0] = g2[0][0]
            x2[0][1] = g2[0][1]
            x2[1][0] = g2[1][0]
            x2[1][1] = g2[1][1]

            i = 0
            while i + 2 < 7:
                liczba = [random.randint(2, 14), random.randint(1, 4)]
                if x1.count(liczba) == 0:
                    x1[i + 2] = liczba

                    i = i + 1

            for i in range(2, 7):
                x2[i][0] = x1[i][0]
                x2[i][1] = x1[i][1]

        if (znacznik == 1):
            x1[0][0] = g1[0][0]
            x1[0][1] = g1[0][1]
            x1[1][0] = g1[1][0]
            x1[1][1] = g1[1][1]
            x1[2][0] = g1[2][0]
            x1[2][1] = g1[2][1]
            x1[3][0] = g1[3][0]
            x1[3][1] = g1[3][1]
            x1[4][0] = g1[4][0]
            x1[4][1] = g1[4][1]

            x2[0][0] = g2[0][0]
            x2[0][1] = g2[0][1]
            x2[1][0] = g2[1][0]
            x2[1][1] = g2[1][1]
            x2[2][0] = g2[2][0]
            x2[2][1] = g2[2][1]
            x2[3][0] = g2[3][0]
            x2[3][1] = g2[3][1]
            x2[4][0] = g2[4][0]
            x2[4][1] = g2[4][1]

            i = 0
            while i + 5 < 7:
                liczba = [random.randint(2, 14), random.randint(1, 4)]
                if x1.count(liczba) == 0:
                    x1[i + 5] = liczba
                    i = i + 1

            for i in range(5, 7):
                x2[i][0] = x1[i][0]
                x2[i][1] = x1[i][1]

        if (znacznik == 2):
            x1[0][0] = g1[0][0]
            x1[0][1] = g1[0][1]
            x1[1][0] = g1[1][0]
            x1[1][1] = g1[1][1]
            x1[2][0] = g1[2][0]
            x1[2][1] = g1[2][1]
            x1[3][0] = g1[3][0]
            x1[3][1] = g1[3][1]
            x1[4][0] = g1[4][0]
            x1[4][1] = g1[4][1]
            x1[5][0] = g1[5][0]
            x1[5][1] = g1[5][1]

            x2[0][0] = g2[0][0]
            x2[0][1] = g2[0][1]
            x2[1][0] = g2[1][0]
            x2[1][1] = g2[1][1]
            x2[2][0] = g2[2][0]
            x2[2][1] = g2[2][1]
            x2[3][0] = g2[3][0]
            x2[3][1] = g2[3][1]
            x2[4][0] = g2[4][0]
            x2[4][1] = g2[4][1]
            x2[5][0] = g2[5][0]
            x2[5][1] = g2[5][1]

            i = 0
            while i + 6 < 7:
                liczba = [random.randint(2, 14), random.randint(1, 4)]
                if x1.count(liczba) == 0:
                    x1[i + 6] = liczba
                    i = i + 1

            for i in range(6, 7):
                x2[i][0] = x1[i][0]
                x2[i][1] = x1[i][1]

        if (znacznik == 3):
            x1[0][0] = g1[0][0]
            x1[0][1] = g1[0][1]
            x1[1][0] = g1[1][0]
            x1[1][1] = g1[1][1]
            x1[2][0] = g1[2][0]
            x1[2][1] = g1[2][1]
            x1[3][0] = g1[3][0]
            x1[3][1] = g1[3][1]
            x1[4][0] = g1[4][0]
            x1[4][1] = g1[4][1]
            x1[5][0] = g1[5][0]
            x1[5][1] = g1[5][1]
            x1[6][0] = g1[6][0]
            x1[6][1] = g1[6][1]

            x2[0][0] = g2[0][0]
            x2[0][1] = g2[0][1]
            x2[1][0] = g2[1][0]
            x2[1][1] = g2[1][1]
            x2[2][0] = g2[2][0]
            x2[2][1] = g2[2][1]
            x2[3][0] = g2[3][0]
            x2[3][1] = g2[3][1]
            x2[4][0] = g2[4][0]
            x2[4][1] = g2[4][1]
            x2[5][0] = g2[5][0]
            x2[5][1] = g2[5][1]
            x2[6][0] = g2[6][0]
            x2[6][1] = g2[6][1]

        wagareki = para(x1, wagareki)
        wagareki = color(x1, wagareki)
        wagareki = straight3(x1, wagareki)

        w1 = wagareki

        wagareki = [0, 0, 0, 0, 0, 0]
        wagareki = para(x2, wagareki)
        wagareki = color(x2, wagareki)

        w2 = wagareki

        if w1[0] == 0:
            u01 = u01 + 1
        if w1[0] == 1:
            u11 = u11 + 1
        if w1[0] == 2:
            u21 = u21 + 1
        if w1[0] == 3:
            u31 = u31 + 1
        if w1[0] == 4:
            u41 = u41 + 1
        if w1[0] == 5:
            u51 = u51 + 1
        if w1[0] == 6:
            u61 = u61 + 1
        if w1[0] == 7:
            u71 = u71 + 1
        if w1[0] == 8:
            u81 = u81 + 1
        if w1[0] == 9:
            u91 = u91 + 1

        for i in range(0, 6):
            if (w1[i] > w2[i]):
                z1 = z1 + 1
                break
            elif (w1[i] < w2[i]):
                z2 = z2 + 1

                break
        if (w1 == w2):
            remis = remis + 1

        wagareki = [0, 0, 0, 0, 0, 0]

    pwing1 = round((z1) * 100 / N, 2)
    prem = round(remis * 100 / N, 2)

    kareta = round(u81 * 100 / N, 2)
    full = round(u71 * 100 / N, 2)
    kolor = round(u61 * 100 / N, 2)
    street = round(u41 * 100 / N, 2)
    trips = round(u31 * 100 / N, 2)
    dwie_pary = round(u21 * 100 / N, 2)
    jedna_para = round(u11 * 100 / N, 2)
    high_card = round(u01 * 100 / N, 2)

    return pwing1, prem, kareta, full, kolor, street, trips, dwie_pary, jedna_para, high_card
